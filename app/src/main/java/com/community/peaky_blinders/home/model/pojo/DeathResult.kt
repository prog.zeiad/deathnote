package com.community.peaky_blinders.home.model.pojo

import com.google.gson.annotations.SerializedName


    data class DeathResult(
            @SerializedName("id")
            val id: String, // 3
            @SerializedName("death_id_owner")
            val deathIdOwner: String, // 123
            @SerializedName("death_title")
            val deathTitle: String, // Death title 3
            @SerializedName("death_desc")
            val deathDesc: String, // Death Des 3
            @SerializedName("death_flag")
            val deathFlag: String // false
    )

