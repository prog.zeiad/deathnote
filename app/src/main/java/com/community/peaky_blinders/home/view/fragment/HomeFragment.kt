package com.community.peaky_blinders.home.view.fragment


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.community.peaky_blinders.R
import com.community.peaky_blinders.home.adapter.MyAdapter
import com.community.peaky_blinders.home.model.pojo.HomeWrapper
import com.community.peaky_blinders.home.model.service.HomeWebService
import com.community.peaky_blinders.home.viewmodel.HomeViewModel
import com.community.peaky_blinders.utility.singleton.RetrofitSingleton
import kotlinx.android.synthetic.main.fragment_home.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"
private var mHomeViewModel: HomeViewModel? = null

class HomeFragment : Fragment() {
//    var mHomeFragment: HomeFragment? = null
//
//
//    fun newInstance(): HomeFragment {
//        if (mHomeFragment == null) {
//            mHomeFragment = HomeFragment()
//        }
//        return mHomeFragment as HomeFragment
//    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view: View = inflater!!.inflate(R.layout.fragment_home, container,
                false)
//        loadingHome_pb.visibility = View.VISIBLE
        obtainViewModel()
        subscribeToUI()
        return view
    }

    private fun obtainViewModel() {
        mHomeViewModel = ViewModelProviders.of(this).get(HomeViewModel::class.java)
        val iHomeService = RetrofitSingleton.getHome("http://dummy.restapiexample.com/api/v1/").create(HomeWebService::class.java)
        mHomeViewModel?.setHomeSevice(iHomeService)
        mHomeViewModel?.callHomeAPI()
    }

    private fun subscribeToUI() {
        mHomeViewModel?.suessHomeJson?.observe(this, Observer<List<HomeWrapper>> { response ->
            if (response?.size != 0)
                item_rv.adapter = MyAdapter(response!!, this!!.activity!!)
            loadingHome_pb.visibility = View.GONE

        })

        mHomeViewModel!!.errorHomeJson?.observe(this, Observer<String> { s ->
            loadingHome_pb.visibility = View.GONE

            Toast.makeText(this!!.activity!!, s, Toast.LENGTH_SHORT).show()
        })
    }

    override fun onDetach() {
        super.onDetach()
        instance = null
    }

    companion object {
        private var instance: HomeFragment? = null

        val managerInstance: HomeFragment
            get() {
                if (instance == null) {
                    instance = HomeFragment()
                }

                return instance as HomeFragment
            }
    }

    val fragmentInstance = HomeFragment.Companion
}