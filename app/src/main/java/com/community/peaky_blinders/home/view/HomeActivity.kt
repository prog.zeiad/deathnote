package com.community.peaky_blinders.home.view

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.community.peaky_blinders.R
import com.community.peaky_blinders.home.view.fragment.HomeFragment
import com.community.peaky_blinders.home.view.fragment.UserNoteFragment
import com.community.peaky_blinders.testPackage.MapFragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_home.*

class HomeActivity : AppCompatActivity() {

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_home -> {
                openFragment(HomeFragment().fragmentInstance.managerInstance)
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_privateNote -> {
                openFragment(UserNoteFragment().fragmentInstance.managerInstance)
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_community -> {
                Toast.makeText(this@HomeActivity, "1", Toast.LENGTH_SHORT).show()
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_map -> {
                openFragment(MapFragment().fragmentInstance.managerInstance)
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        openFragment(HomeFragment().fragmentInstance.managerInstance)
    }

    fun openFragment(mFragment: Fragment) {
        supportFragmentManager
                .beginTransaction()
                .replace(R.id.mainContainer, mFragment, "rageComicList")
                .commit()
    }
}
