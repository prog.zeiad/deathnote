package com.community.peaky_blinders.home.model.pojo

import com.google.gson.annotations.SerializedName

data class HomeWrapper(
        @SerializedName("id")
        val id: String, // 60
        @SerializedName("employee_name")
        val employeeName: String, // wefwef
        @SerializedName("employee_salary")
        val employeeSalary: String, // 34
        @SerializedName("employee_age")
        val employeeAge: String, // 23452
        @SerializedName("profile_image")
        val profileImage: String,
        @SerializedName("userLike")
        var userLike: Boolean = false
)