package com.community.peaky_blinders.home.model.pojo

import com.google.gson.annotations.SerializedName

class JsonWrapper(
        @SerializedName("stutes")
        val stutes: Boolean, // true
        @SerializedName("deathResult")
        val deathResult: List<DeathResult>
)
