package com.community.peaky_blinders.home.view.fragment

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import androidx.fragment.app.DialogFragment
import com.community.peaky_blinders.R

class PrivateNoteDialogFragment : DialogFragment(), View.OnClickListener {


    lateinit var userNoteEd: EditText
    lateinit var saveBt: Button
    lateinit var cancelBt: Button
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.fraglayout, container)
        userNoteEd = rootView.findViewById(R.id.userNote_ed)
        saveBt = rootView.findViewById(R.id.save_bt)
        cancelBt = rootView.findViewById(R.id.cancel_bt)
        saveBt.setOnClickListener(this)
        cancelBt.setOnClickListener(this)
        return rootView
    }

    override fun onClick(v: View?) {
        val intent = Intent()
        when (v?.id) {
            R.id.save_bt -> {
                intent.putExtra("data", "" + userNoteEd.text.toString())
                targetFragment!!.onActivityResult(targetRequestCode, 1, intent)
                dismiss()
            }
            R.id.cancel_bt -> {
                dismiss()
            }
        }
    }
}