package com.community.peaky_blinders.home.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.community.peaky_blinders.R
import com.community.peaky_blinders.home.model.pojo.HomeWrapper
import kotlinx.android.synthetic.main.item_rv_layout.view.*

class MyAdapter(private val iItemList: List<HomeWrapper>, private val context: Context) : RecyclerView.Adapter<MyAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_rv_layout, parent, false))
    }

    override fun getItemCount(): Int {
        //   Log.e("/*/**/", "" + iItemList.size)
        return iItemList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder?.orderTitle_tv?.text = iItemList[position].employeeName
        holder?.orderDescription_tv?.text = iItemList[position].employeeSalary
        if (iItemList[position].userLike) {
            holder.finishTask_iv.setImageResource(R.drawable.ic_like_fill)
            holder.mission_emjo.setImageResource(R.drawable.aliviado)
        } else {
            holder.finishTask_iv.setImageResource(R.drawable.ic_like)
            holder.mission_emjo.setImageResource(R.drawable.neutro)
        }
        holder?.finishTask_iv.setOnClickListener {
            holder.finishTask_iv.setImageResource(R.drawable.ic_like_fill)
            holder.mission_emjo.setImageResource(R.drawable.aliviado)
            iItemList[position].userLike = true
        }
    }


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val orderTitle_tv = itemView.orderTitle_tv
        val orderDescription_tv = itemView.orderDescription_tv
        val finishTask_iv = itemView.finishTask_iv
        val mission_emjo = itemView.mission_emjo

    }
}