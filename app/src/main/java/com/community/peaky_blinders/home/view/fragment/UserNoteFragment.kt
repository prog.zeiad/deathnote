package com.community.peaky_blinders.home.view.fragment


import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.community.peaky_blinders.R
import com.community.peaky_blinders.room.model.Word
import com.example.android.roomwordssample.WordViewModel
import com.google.android.material.floatingactionbutton.FloatingActionButton
import java.text.SimpleDateFormat
import java.util.*

class UserNoteFragment : Fragment(), View.OnClickListener {
    lateinit var userNote_fb: FloatingActionButton
    private lateinit var wordViewModel: WordViewModel
    private var adapter: WordListAdapter? = null
    private var newWordActivityRequestCode = 1
    var recyclerView: RecyclerView? = null
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view: View = inflater!!.inflate(R.layout.fragment_user_note, container,
                false)
        userNote_fb = view.findViewById(R.id.userNote_fb)
        userNote_fb.setOnClickListener(this)
        recyclerView = view.findViewById(R.id.item_rv) as RecyclerView

        recyclerView!!.layoutManager = LinearLayoutManager(this!!.context!!)
        wordViewModel = ViewModelProviders.of(this).get(WordViewModel::class.java)
        return view
    }

    override fun onStart() {
        super.onStart()
        wordViewModel.allWords.observe(this, Observer { words ->
            // Update the cached copy of the words in the adapter.
            words?.let {
                adapter = WordListAdapter(this!!.context!!, it)
                recyclerView?.adapter = adapter

            }
        })

    }

    override fun onDetach() {
        super.onDetach()
        instance = null
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.userNote_fb -> {
                showEditDialog()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, intentData: Intent?) {
        super.onActivityResult(requestCode, resultCode, intentData)
        if (requestCode == newWordActivityRequestCode) {
            if (intentData?.getStringExtra("data")?.trim() == "") {
                Toast.makeText(context, "We Can't Save Empty Task", Toast.LENGTH_SHORT).show()
                return
            }
            intentData?.let { data ->
                val word = Word(0, data.getStringExtra("data"), getCurrentDate())
                wordViewModel.insert(word)
            }
        } else {
            Toast.makeText(context,
                    "empty_not_saved",
                    Toast.LENGTH_LONG
            ).show()
        }
    }


    private fun getCurrentDate(): String {
        val sdf = SimpleDateFormat("dd/M/yyyy - hh:mm:ss")
        return sdf.format(Date())
    }

    private fun showEditDialog() {
        val fm = activity!!.supportFragmentManager
        val genericDialogFragment = PrivateNoteDialogFragment()
        genericDialogFragment.setTargetFragment(this, 1)
        genericDialogFragment.show(fm, "fragment_edit_name")
    }

    //--------------------------------------------------------------------------------------------------
    companion object {
        private var instance: UserNoteFragment? = null

        val managerInstance: UserNoteFragment
            get() {
                if (instance == null) {
                    instance = UserNoteFragment()
                }

                return instance as UserNoteFragment
            }
    }

    val fragmentInstance = UserNoteFragment.Companion
}
