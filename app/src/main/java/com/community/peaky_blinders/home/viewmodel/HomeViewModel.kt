package com.community.peaky_blinders.home.viewmodel


import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.community.peaky_blinders.home.model.pojo.HomeWrapper
import com.community.peaky_blinders.home.model.service.HomeWebService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class HomeViewModel : ViewModel() {
    var homeWebService: HomeWebService? = null
    var suessHomeJson: MutableLiveData<List<HomeWrapper>>? = null
    var errorHomeJson: MutableLiveData<String>? = null

    init {
        suessHomeJson = MutableLiveData()
        errorHomeJson = MutableLiveData()
    }

    fun setHomeSevice(mHomeService: HomeWebService) {
        homeWebService = mHomeService
    }

    fun callHomeAPI() {
        homeWebService?.getHomeJson()?.enqueue(object : Callback<List<HomeWrapper>> {
            override fun onFailure(call: Call<List<HomeWrapper>>, t: Throwable) {
                errorHomeJson?.value = "error Json;;;ll"
                Log.e("/**/","asd"+t.toString())
            }

            override fun onResponse(call: Call<List<HomeWrapper>>, response: Response<List<HomeWrapper>>) {
              //  suessHomeJson?.value = response.body()

                if (response.raw().code() == 200) {
                    suessHomeJson?.value = response.body()
                  //  Log.e("/**/","asd"+response.body().toString())
                } else {
                    errorHomeJson?.value = "error Json"
                }
            }


        })

    }


}
