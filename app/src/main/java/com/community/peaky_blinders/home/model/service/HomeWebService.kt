package com.community.peaky_blinders.home.model.service

import com.community.peaky_blinders.home.model.pojo.HomeWrapper
import retrofit2.Call
import retrofit2.http.GET

interface HomeWebService {
    @GET("employees")
    fun getHomeJson(): Call<List<HomeWrapper>>
}