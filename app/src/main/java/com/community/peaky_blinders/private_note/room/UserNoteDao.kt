package com.community.peaky_blinders.private_note.room

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import io.reactivex.Completable
import io.reactivex.Flowable

@Dao
interface UserNoteDao {
    @Query("select * from UserNote")
    fun getAll(): Flowable<List<UserNoteEntity>>
//    fun getAll(): LiveData<List<UserNoteEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertItem(vararg users: UserNoteEntity): Completable
}