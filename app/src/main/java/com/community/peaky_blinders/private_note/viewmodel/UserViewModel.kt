/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.community.peaky_blinders.private_note.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.community.peaky_blinders.private_note.room.UserNoteDao
import com.community.peaky_blinders.private_note.room.UserNoteEntity
import io.reactivex.Completable
import io.reactivex.Flowable

/**
 * View Model for the [UserActivity]
 */
class UserViewModel(private val dataSource: UserNoteDao) : ViewModel() {
    val successResponseData: MutableLiveData<List<UserNoteEntity>> = MutableLiveData()
    val errorResponseData: MutableLiveData<String> = MutableLiveData()
    /**
     * Get the user name of the user.

     * @return a [Flowable] that will emit every time the user name has been updated.
     */
    // for every emission of the user, get the user name
//    fun userNamesa(): List<UserNoteEntity> {
//        return dataSource.getAll()
//    }
    fun userName(): Flowable<List<UserNoteEntity>> {
        return dataSource.getAll()

    }

    /**
     * Update the user name.
     * @param userName the new user name
     * *
     * @return a [Completable] that completes when the user name is updated
     */
    fun updateUserName(userName: UserNoteEntity): Completable {

        return dataSource.insertItem(userName)
    }

//    companion object {
//        // using a hardcoded value for simplicity
//        const val USER_ID = 1
//    }
}
