package com.community.peaky_blinders.private_note.room

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "UserNote")
data class UserNoteEntity(
        //@PrimaryKey var uid: Int,
        @PrimaryKey (autoGenerate = true)
        var uid: Int,
        @ColumnInfo(name = "user_note")
        var userNote: String?,
        @ColumnInfo(name = "note_flag")
        var noteFlag: Boolean?
)