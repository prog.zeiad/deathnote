package com.community.peaky_blinders.utility.singleton

import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitSingleton {

    // val API_BASE_URL = "https://reqres.in/api/"
    private var retrofit: Retrofit? = null
    private var retrofitHome: Retrofit? = null

    fun get(baseUrl: String): Retrofit {
        val client = OkHttpClient.Builder().build()
        if (retrofit == null) {
            retrofit = Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(client)
                    .build()
        }

        return retrofit!!
    }

    fun getHome(baseUrl: String): Retrofit {
        val clientHome = OkHttpClient.Builder().build()
        if (retrofitHome == null) {
            retrofitHome = Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(clientHome)
                    .build()
        }

        return retrofitHome!!
    }
}
