package com.community.peaky_blinders.utility

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo

class HelperUtility {
     fun checkNetworkConnection(context: Context): Boolean {
        val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        var activeNetworkInfo: NetworkInfo?
        activeNetworkInfo = cm.activeNetworkInfo
         val isWiFi: Boolean = activeNetworkInfo?.type == ConnectivityManager.TYPE_WIFI
        return activeNetworkInfo != null && activeNetworkInfo.isConnected
    }
}