package com.community.peaky_blinders.utility

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.Gravity
import android.view.ViewGroup
import android.view.Window
import com.community.peaky_blinders.R


class ProgressDialog
//private IndicatorStyle style;

(private val context: Context)//  this.style = style;
{

    fun show(): Dialog {
        // Create custom dialog object
        val dialog = Dialog(context)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.getWindow()?.setLayout(ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT)
        dialog.getWindow()?.setGravity(Gravity.CENTER)
        dialog.getWindow()?.setBackgroundDrawable(
                ColorDrawable(Color.TRANSPARENT))
        // Include dialog.xml file

        dialog.setContentView(R.layout.progress_loader)


        dialog.setCancelable(false)
        dialog.show()
        return dialog
    }
}
