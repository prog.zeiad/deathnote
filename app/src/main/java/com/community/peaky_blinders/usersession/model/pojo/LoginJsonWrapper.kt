package com.community.peaky_blinders.usersession.model.pojo

import com.google.gson.annotations.SerializedName

data class LoginJsonWrapper(
        @SerializedName("status")
        val status: Boolean, // true
        @SerializedName("result")
        val result: Result
)