package com.community.peaky_blinders.usersession.model.pojo

class ConnectionModel(val type: Int, val isConnected: Boolean)