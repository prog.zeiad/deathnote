package com.community.peaky_blinders.usersession.view

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.community.peaky_blinders.home.view.HomeActivity
import com.community.peaky_blinders.usersession.model.pojo.ConnectionModel
import com.community.peaky_blinders.usersession.model.pojo.TestJson
import com.community.peaky_blinders.usersession.model.service.WebService
import com.community.peaky_blinders.usersession.viewmodel.ConnectionLiveData
import com.community.peaky_blinders.usersession.viewmodel.LoginViewModel
import com.community.peaky_blinders.utility.singleton.RetrofitSingleton
import kotlinx.android.synthetic.main.activity_main.*


class LoginActivity : AppCompatActivity(), View.OnClickListener {
    private var mViewModel: LoginViewModel? = null
    private var connectionLiveData: ConnectionLiveData? = null
   // private var progressDialog: Dialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(com.community.peaky_blinders.R.layout.activity_main)

        login_tv.setOnClickListener(this)
        singUp_tv.setOnClickListener(this)
        obtainViewModel()
        subscribeToUI()
        getConnectionObserver()

    }

//    fun showProgressDialog() {
//        progressDialog = ProgressDialog(this@LoginActivity).show()
//    }
//
//    fun hideProgressDialog() {
//        progressDialog?.dismiss()
//    }

    override fun onClick(itemView: View?) {
        when (itemView?.id) {
            com.community.peaky_blinders.R.id.login_tv -> {
                // loading_pb.visibility = View.VISIBLE
                if (getConnectionObserver()) {
                    mViewModel!!.setLoginParams(userName_ed?.text.toString(), userPass_ed?.text.toString())
                    mViewModel!!.setLogin(this@LoginActivity)
                } else {
                    val intent = Intent(this@LoginActivity, RegisterActivity::class.java)
                }

            }
            com.community.peaky_blinders.R.id.singUp_tv -> {
                val intent = Intent(this@LoginActivity, RegisterActivity::class.java)
                startActivity(intent)
                // Toast.makeText(this, " SignUp" + userPass_ed.text.toString(), Toast.LENGTH_SHORT).show()
            }

        }
    }

    //------------------------------------

    private fun obtainViewModel() {
        mViewModel = ViewModelProviders.of(this).get(LoginViewModel::class.java)
        connectionLiveData = ConnectionLiveData(applicationContext)
        val iLoginWebService = RetrofitSingleton.get("https://reqres.in/api/").create(WebService::class.java)
        mViewModel!!.setWebService(iLoginWebService)
    }

    private fun subscribeToUI() {
       // hideProgressDialog()
        mViewModel!!.successResponseData.observe(this, Observer<TestJson> { s1 ->
            //      loading_pb.visibility = View.GONE
           // hideProgressDialog()
          //  Toast.makeText(this@LoginActivity, "k" + s1, Toast.LENGTH_SHORT).show()
          startActivity(Intent(this, HomeActivity::class.java))
           finish()
        })

        mViewModel!!.errorResponseData.observe(this, Observer<String> { s ->
            loading_pb.visibility = View.GONE
            Toast.makeText(this@LoginActivity, "k" + s, Toast.LENGTH_SHORT).show()
        })
    }

    private fun getConnectionObserver(): Boolean {
        var isConnect = false
        connectionLiveData?.observe(this, object : Observer<ConnectionModel> {
            override fun onChanged(connection: ConnectionModel) {
                /* every time connection state changes, we'll be notified and can perform action accordingly */
                if (connection.isConnected) {
                    when (connection.type) {
                        1 ->
                            //return true
                            isConnect = true
                        //Toast.makeText(this@LoginActivity, "Wifi on", Toast.LENGTH_SHORT).show()
                        2 ->
                            isConnect = true
                        // Toast.makeText(this@LoginActivity, "Data On", Toast.LENGTH_SHORT).show()
                    }
                } else {
                   // hideProgressDialog()
                    isConnect = false
                    //  Toast.makeText(this@LoginActivity, "No dat", Toast.LENGTH_SHORT).show()
                }
            }
        })
        return isConnect
    }
}
