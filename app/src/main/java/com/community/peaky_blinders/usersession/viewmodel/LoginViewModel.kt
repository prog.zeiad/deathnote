package com.community.peaky_blinders.usersession.viewmodel

import android.app.Dialog
import android.content.Context
import android.text.TextUtils.isEmpty
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.community.peaky_blinders.usersession.model.pojo.SignInParams
import com.community.peaky_blinders.usersession.model.pojo.TestJson
import com.community.peaky_blinders.usersession.model.service.WebService
import com.community.peaky_blinders.utility.ProgressDialog
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LoginViewModel : ViewModel() {

    private var mWebService: WebService? = null
    val successResponseData: MutableLiveData<TestJson> = MutableLiveData()
    val errorResponseData: MutableLiveData<String> = MutableLiveData()
    private var iParams: SignInParams? = null
    private var progressDialog: Dialog? = null

    private val isDataValid: Boolean
        get() {
            val emailValid = !isEmpty(iParams!!.email)
            val passValid = !isEmpty(iParams!!.passowrd.trim()) && iParams!!.passowrd.trim().length >= 2
            if (!emailValid) {
                errorResponseData.value = "من فضلك ادخل البريد الالكترونى"
            } else if (!passValid) {
                errorResponseData.value = "من فضلك ادخل كلمه المرور"

            }
            return emailValid && passValid
        }

    fun setWebService(mWebService: WebService) {
        this.mWebService = mWebService
    }
    private fun showProgressDialog(context: Context) {
        progressDialog = ProgressDialog(context).show()
    }

    fun hideProgressDialog() {
        progressDialog?.dismiss()
    }
    fun setLoginParams(aEmail: String, aPass: String) {
        iParams = SignInParams()
        iParams!!.email = aEmail
        iParams!!.passowrd = aPass
    }


//    private fun checkNetwork() {
//        when (NetworkUtil.getConnectivityStatus(ApplicationContext()) {
//            OFFLINE -> valLoginData.setValue("no internet")
//            WIFI_CONNECTED_WITHOUT_INTERNET -> valLoginData.setValue("no internet")
//            MOBILE_DATA_CONNECTED, WIFI_CONNECTED_WITH_INTERNET -> valLoginData.setValue("connected")
//            UNKNOWN -> valLoginData.setValue("no internet")
//        }
//    }

    fun setLogin(context: Context) {
        if (isDataValid) {
//            if (HelperUtility().checkNetworkConnection(MyApplication.getAppContext()))
            showProgressDialog(context)
            mWebService!!.getLogin().enqueue(object : Callback<TestJson> {

                    override fun onResponse(call: Call<TestJson>, response: Response<TestJson>) {
                        if (response.raw().code() == 200) {
                            successResponseData.setValue(response.body())
                            hideProgressDialog()
                        } else {
                            errorResponseData.value = "Response error"
                            hideProgressDialog()
                        }
                    }

                override fun onFailure(call: Call<TestJson>, t: Throwable) {
                    errorResponseData.value = "Response error"
                    hideProgressDialog()
                }
            })
        }

    }
}
