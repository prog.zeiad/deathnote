package com.community.peaky_blinders.usersession.view

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import com.community.peaky_blinders.R

class SplashActivity : Activity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        Handler().postDelayed({
            startActivity(Intent(this@SplashActivity, LoginActivity::class.java))
            finish()
        }, 3000)

    }

}
