package com.community.peaky_blinders.usersession.model.pojo

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by m.ahmed on 4/1/2018.
 */

 class SignInParams {

    @SerializedName("email_phone")
    @Expose
    var email: String = ""

    @SerializedName("password")
    @Expose
    var passowrd: String = ""
}
