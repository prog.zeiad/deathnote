package com.community.peaky_blinders.usersession.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.community.peaky_blinders.R

class RegisterActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
    }
}
