package com.community.peaky_blinders.usersession.model.service

import com.community.peaky_blinders.usersession.model.pojo.TestJson
import retrofit2.Call
import retrofit2.http.GET

interface WebService {

//    @get:POST("HomeJsonPresenterImp")
//    @FormUrlEncoded //-----used to add pram to method
//    val categories: Call<LoginJsonWrapper>    @get:POST("HomeJsonPresenterImp")
//    @FormUrlEncoded //-----used to add pram to method
//    fun getLogin(@Field("email_phone") email: String, @Field("password") pass: String): Call<TestJson>

    @GET("users")
    fun getLogin(): Call<TestJson>


}
