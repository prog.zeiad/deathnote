package com.community.peaky_blinders.usersession.model.pojo

import com.google.gson.annotations.SerializedName

data class Result(
        @SerializedName("userName")
        val userName: String, // zeiad
        @SerializedName("userId")
        val userId: String, // 123456
        @SerializedName("userPass")
        val userPass: String, // 123456
        @SerializedName("userRole")
        val userRole: String, // Kira
        @SerializedName("userPhone")
        val userPhone: String // 010
)