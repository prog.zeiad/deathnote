package com.community.peaky_blinders.usersession.model.pojo

import com.google.gson.annotations.SerializedName

data class TestJson(
    @SerializedName("page")
    val page: Int, // 1
    @SerializedName("per_page")
    val perPage: Int, // 3
    @SerializedName("total")
    val total: Int, // 12
    @SerializedName("total_pages")
    val totalPages: Int, // 4
    @SerializedName("data")
    val `data`: List<Data>
) {
    data class Data(
        @SerializedName("id")
        val id: Int, // 3
        @SerializedName("first_name")
        val firstName: String, // Emma
        @SerializedName("last_name")
        val lastName: String, // Wong
        @SerializedName("avatar")
        val avatar: String // https://s3.amazonaws.com/uifaces/faces/twitter/olegpogodaev/128.jpg
    )
}