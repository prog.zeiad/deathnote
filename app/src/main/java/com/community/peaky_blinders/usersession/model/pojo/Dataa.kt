package com.community.peaky_blinders.usersession.model.pojo

import com.google.gson.annotations.SerializedName

data class Dataa(
        @SerializedName("id")
        val id: Int, // 3
        @SerializedName("first_name")
        val firstName: String, // Emma
        @SerializedName("last_name")
        val lastName: String, // Wong
        @SerializedName("avatar")
        val avatar: String // https://s3.amazonaws.com/uifaces/faces/twitter/olegpogodaev/128.jpg
)