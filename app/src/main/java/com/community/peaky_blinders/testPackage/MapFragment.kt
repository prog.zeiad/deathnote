package com.community.peaky_blinders.testPackage


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.community.peaky_blinders.R
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions

var mapFragment: SupportMapFragment? = null
private val LOCATION_REQUEST_CODE = 101
private var mMap: GoogleMap? = null

class MapFragment : Fragment(), OnMapReadyCallback {


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view: View = inflater!!.inflate(R.layout.activity_maps, container,
                false)

        mapFragment = fragmentManager?.findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment?.getMapAsync(this)
        return view
    }

    override fun onMapReady(googleMap: GoogleMap?) {
        mMap = googleMap

        // Add a marker in Sydney and move the camera
        val sydney = LatLng(-34.0, 151.0)
        mMap?.addMarker(MarkerOptions().position(sydney).title("Marker in Sydney"))
        mMap?.moveCamera(CameraUpdateFactory.newLatLng(sydney))
    }
    override fun onDetach() {
        super.onDetach()
        instance = null
    }

    companion object {
        private var instance: MapFragment? = null

        val managerInstance: MapFragment
            get() {
                if (instance == null) {
                    instance = MapFragment()
                }

                return instance as MapFragment
            }
    }

    val fragmentInstance = MapFragment.Companion
}
